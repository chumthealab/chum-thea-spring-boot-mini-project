CREATE TABLE IF NOT EXISTS tb_category (
  cat_id serial PRIMARY KEY NOT NULL,
  cat_title varchar(255)
);

CREATE TABLE IF NOT EXISTS tb_article (
  id serial PRIMARY KEY NOT NULL,
  title varchar(255),
  description text,
   author varchar(255),
  thumbnail varchar(255),
  cat_id int4 REFERENCES tb_category (cat_id) ON UPDATE CASCADE ON DELETE CASCADE
);