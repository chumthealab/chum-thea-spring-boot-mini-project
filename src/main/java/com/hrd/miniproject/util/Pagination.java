package com.hrd.miniproject.util;

import com.hrd.miniproject.models.Article;

import java.util.List;

public class Pagination {

    private int page;
    private int limit;
    private int totalRecord;
    private int totalPage;
    private int pageToShow;
    private int recordToShow;
    private int offset;

    public Pagination() {
    }

    public Pagination(int page, int limit, int totalRecord, int totalPage, int pageToShow, int recordToShow, int offset) {
        this.page = page;
        this.limit = limit;
        this.totalRecord = totalRecord;
        this.totalPage = totalPage;
        this.pageToShow = pageToShow;
        this.recordToShow = recordToShow;
        this.offset = offset;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalRecord,int limit) {
    }

    public int getPageToShow() {
        return pageToShow;
    }

    public void setPageToShow(int pageToShow) {
        this.pageToShow = pageToShow;
    }

    public int getRecordToShow() {
        return recordToShow;
    }

    public void setRecordToShow(int recordToShow) {
        this.recordToShow = recordToShow;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "page=" + page +
                ", limit=" + limit +
                ", totalRecord=" + totalRecord +
                ", totalPage=" + totalPage +
                ", pageToShow=" + pageToShow +
                ", recordToShow=" + recordToShow +
                ", offset=" + offset +
                '}';
    }
}
