package com.hrd.miniproject.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {

    private int id;
    @NotBlank(message = "Titlte is requrid")
    private String title;
    @NotEmpty
    private String description;
    @NotBlank
    @Size(min = 3, max = 10)
    private String author;

    private String thumbnail;
    private int cat_id;
    private Category category;

    public Article(){}

    public Article(@NotNull Integer id, @NotBlank String title, @NotEmpty String description, @NotBlank @Size(min = 3, max = 8) String author, String createdDate, String thumbnail, int cat_id) {
        this.title = title;
        this.description = description;
        this.author = author;
        this.thumbnail = thumbnail;
        this.cat_id = cat_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return id + title + description + thumbnail + cat_id ;
    }
}
