package com.hrd.miniproject.controllers;

import com.hrd.miniproject.models.Category;
import com.hrd.miniproject.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategoryController {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @RequestMapping("/cat/add")
    public String addCat(Model model){
        model.addAttribute("cat", new Category());
        model.addAttribute("categories", categoryRepository.findAll());
        model.addAttribute("isAdd", true);
        return "cate";
    }

    @PostMapping("/cat/add")
    public String saveAdd(@ModelAttribute("cat") Category category){
        categoryRepository.insert(category);
        return "redirect:/cat/add";
    }

    @RequestMapping("/cat/view/{id}")
    public String findById(@PathVariable Integer id, Model model){
        model.addAttribute("cat",categoryRepository.findById(id));
        model.addAttribute("articles", categoryRepository.findArticleByCatTitle(categoryRepository.findById(id).getCat_title()));
        return "cat_detail";
    }

    @RequestMapping("/cat/delete/{id}")
    public String deleteById(@PathVariable Integer id){
        categoryRepository.deleteById(id);
        return "redirect:/cat/add";
    }

    @RequestMapping("/cat/update/{id}")
    public String update(@PathVariable Integer id, ModelMap modelMap){
        modelMap.addAttribute("cat", categoryRepository.findById(id));
        modelMap.addAttribute("isAdd", false);
        return "cate";
    }

    @PostMapping("/cat/update")
    public String saveUpdate(@ModelAttribute Category category){

        categoryRepository.update(category);
        return "redirect:/cat/add";
    }
}
