package com.hrd.miniproject.controllers;

import com.hrd.miniproject.models.Article;
import com.hrd.miniproject.repositories.ArticleRepository;
import com.hrd.miniproject.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class HomeController {

    private ArticleRepository articleRepository;
    private CategoryRepository categoryRepository;

    private int currentPage = 1;
    private int scurrentPage = 1;
    private int limitRow = 5;
    private int totalPage;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @RequestMapping("/")
    public String index(Model model){

        List<Article> articles = articleRepository.articlePage(currentPage, limitRow);

        int totalRecord = articleRepository.findAll().size();
        totalPage = totalRecord % limitRow == 0 ? totalRecord / limitRow : totalRecord / limitRow + 1;

        model.addAttribute("isSearch", false);
        model.addAttribute("articles", articles);
        model.addAttribute("categories", categoryRepository.findAll());


        model.addAttribute("currentPage", currentPage);
        model.addAttribute("articles", articles);

        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("limitRow", limitRow);
        return "index";
    }

    @RequestMapping("/article/add")
    public String add(ModelMap modelMap){
        modelMap.addAttribute("article", new Article());
        modelMap.addAttribute("categories", categoryRepository.findAll());
        modelMap.addAttribute("isAdd", true);
        return "add_page";
    }

    @PostMapping("/article/add")
    public String saveAdd(@Valid @ModelAttribute("article") Article article,
                          BindingResult bindingResult,
                          ModelMap modelMap,
                          @RequestParam("file")MultipartFile file ){
        String fileName;

        if (bindingResult.hasErrors()) {
            System.out.println("Has Error");
            modelMap.addAttribute("article", article);
            modelMap.addAttribute("categories", categoryRepository.findAll());
            modelMap.addAttribute("isAdd", true);
            return "add_page";
        }

        if(!file.isEmpty()){
            fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
            try {
                Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images", fileName));
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setThumbnail(fileName);
        }else{
            System.out.println("not image");
        }
        System.out.println("success");
        articleRepository.insert(article);
        return "redirect:/";
    }


    @RequestMapping("/article/view/{id}")
    public String findById(@PathVariable Integer id, ModelMap modelMap){
        Article article = articleRepository.findById(id);
        modelMap.addAttribute("article", article);
        return "article_detail";
    }

    @RequestMapping("/article/delete/{id}")
    public String deleteById(@PathVariable Integer id){
        articleRepository.deleteById(id);
        return "redirect:/";
    }

    @RequestMapping("/article/update/{id}")
    public String update(@PathVariable Integer id, ModelMap modelMap){
        modelMap.addAttribute("article", articleRepository.findById(id));

        modelMap.addAttribute("categories", categoryRepository.findAll());
        modelMap.addAttribute("isAdd", false);
        System.out.println();
        return "add_page";
    }

    @PostMapping("/article/update")
    public String saveUpdate(@Valid  @ModelAttribute Article article, @RequestParam("file") MultipartFile file,ModelMap modelMap, BindingResult bindingResult){

        String fileName;

        if(!file.isEmpty()){
            fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
            try {
//                System.out.println();
//                Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "images", fileName));
                System.out.println("Img updated");
                fileName = UUID.randomUUID().toString() + file.getOriginalFilename();
                Files.copy(file.getInputStream(), Paths.get(System.getProperty("user.dir") + File.separator+"src" + File.separator+"main"+ File.separator+"resources"+ File.separator+"images" + File.separator, fileName));
                article.setThumbnail(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(fileName + "filename");
            article.setThumbnail(fileName);
        }else{
            article.setThumbnail("");
        }

        if (bindingResult.hasErrors()) {
            modelMap.addAttribute("article", article);
            modelMap.addAttribute("isAdd", false);
            return "article/add";
        }

        articleRepository.update(article);
        return "redirect:/";
    }

    List<Article> allArticle;

    public List<Article> getArticles(int currentPage, int limitRow){
        List<Article> articleList = new ArrayList<>();
        int k = 0;
        for(int i = (currentPage - 1)* limitRow; i < allArticle.size() ; i++){
            if(k < limitRow) {articleList.add(allArticle.get(i));}
            k++;
        }
        return articleList;
    }


    @RequestMapping("/article/search")
    public String search(@RequestParam("title") String title, @RequestParam("cat_id") Integer cat_id, Model model){

         allArticle = articleRepository.findArticleByTitleCatAll(title, cat_id);

        int totalRecord = articleRepository.findArticleByTitleCatAll(title, cat_id).size();
        int totalPage = totalRecord % limitRow == 0 ? totalRecord / limitRow : totalRecord / limitRow + 1;

        System.out.println(getArticles(scurrentPage, limitRow).size());
        model.addAttribute("articles",  getArticles(scurrentPage, limitRow));
        model.addAttribute("categories", categoryRepository.findAll());
        System.out.println();
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("limitRow", limitRow);
        return "search_page";
    }

    @RequestMapping("/article/search/page")
    public String searchPage(Model model){
        System.out.println(getArticles(scurrentPage, limitRow).size());
        model.addAttribute("articles",  getArticles(scurrentPage, limitRow));
        System.out.println();
        model.addAttribute("categories", categoryRepository.findAll());

        model.addAttribute("currentPage", currentPage);
        model.addAttribute("totalPage", totalPage);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("limitRow", limitRow);
        return "search_page";
    }



    @RequestMapping("/page")
    public String page(@RequestParam Integer page){
        System.out.println(page + " page");
        currentPage = page;
        return "redirect:/";
    }


    @RequestMapping("/spage")
    public String searchPage(@RequestParam Integer page){
        System.out.println(page + "page ");
        scurrentPage = page;
        return "redirect:/article/search/page";
    }

}

