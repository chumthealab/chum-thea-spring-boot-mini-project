package com.hrd.miniproject.repositories.providers;

import com.hrd.miniproject.models.Article;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {

    public String findAll(){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, c.cat_id, c.cat_title");
            FROM("tb_article as a");
            INNER_JOIN("tb_category as c on a.cat_id = c.cat_id ");
        }}.toString();
    }

    public String insert(Article article){
        return new SQL(){{
            INSERT_INTO("tb_article");
            VALUES("title", "'" + article.getTitle() + "'");
            VALUES("description", "'" + article.getDescription() + "'");
            VALUES("author", "'" + article.getAuthor() + "'");
            VALUES("thumbnail", "'" + article.getThumbnail() + "'");
            VALUES("cat_id", "'" + article.getCat_id() + "'");
        }}.toString();
    }

    public String findById(Integer id){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, c.cat_id, c.cat_title");
            FROM("tb_article as a");
            INNER_JOIN("tb_category as c on a.cat_id = c.cat_id ");
            WHERE("a.id = #{id}");
        }}.toString();
    }

    public String deleteById(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_article");
            WHERE("id = #{id}");
        }}.toString();
    }

    public String update(Article article){
        return new SQL(){{
            UPDATE("tb_article");
            SET("title = " + "'" + article.getTitle() +"'");
            SET("description = " + "'" + article.getDescription() +"'");
            SET("author = " + "'" + article.getAuthor() +"'");
            SET("thumbnail = " + "'" + article.getThumbnail() +"'");
            SET("cat_id = " + "'" + article.getCat_id() +"'");
            WHERE("id = " + "'" + article.getId() + "'");
        }}.toString();
    }

    public String findArticleByTitle(String title){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, c.cat_id, c.cat_title");
            FROM("tb_article as a");
            INNER_JOIN("tb_category as c on a.cat_id = c.cat_id ");
            WHERE("LOWER(a.title) LIKE '%' || #{title} || '%'");
            ORDER_BY("a.id");
        }}.toString();
    }

    public String findArticleByTitleCat(@Param("title") String title,@Param("catId") Integer catId){
        String sql = new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, c.cat_id, c.cat_title");
            FROM("tb_article as a");
            INNER_JOIN("tb_category as c on a.cat_id = c.cat_id ");
            WHERE("c.cat_id = #{catId} AND LOWER(a.title) LIKE '%' || #{title} || '%'");
            ORDER_BY("a.id LIMIT #{limitRow} OFFSET (#{currentPage} - 1) * #{limitRow}");
        }}.toString();
        return sql;
    }

    public String findArticleByTitleCatAll(@Param("title") String title,@Param("catId") Integer catId){
        String sql = new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, c.cat_id, c.cat_title");
            FROM("tb_article as a");
            INNER_JOIN("tb_category as c on a.cat_id = c.cat_id ");
            WHERE("c.cat_id = #{catId} AND LOWER(a.title) LIKE '%' || #{title} || '%'");
            ORDER_BY("a.id ");
        }}.toString();
        return sql;
    }


    public String articlePage(@Param("currentPage") Integer currentPage,@Param("limitRow") Integer limitRow){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, c.cat_id, c.cat_title");
            FROM("tb_article as a");
            INNER_JOIN("tb_category as c on a.cat_id = c.cat_id ");
            ORDER_BY("a.id");
            ORDER_BY("a.id LIMIT #{limitRow} OFFSET (#{currentPage} - 1) * #{limitRow}");
        }}.toString();
    }


}
