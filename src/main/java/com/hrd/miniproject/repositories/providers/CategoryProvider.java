package com.hrd.miniproject.repositories.providers;

import com.hrd.miniproject.models.Category;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String findAll(){
        return new SQL(){{
            SELECT("cat_id, cat_title");
            FROM("tb_category");
        }}.toString();
    }

    public String insert(Category category){
        return new SQL(){{
            INSERT_INTO("tb_category");
            VALUES("cat_title", "'" + category.getCat_title() + "'");
        }}.toString();
    }

    public String findById(Integer id){
        return new SQL(){{
            SELECT("cat_id, cat_title");
            FROM("tb_category");
            WHERE("cat_id = #{id}");
        }}.toString();
    }

    public String findArticleByCatTitle(String title){
        return new SQL(){{
            SELECT("a.id, a.title, a.description, a.author, a.thumbnail, c.cat_id, c.cat_title");
            FROM("tb_article as a");
            INNER_JOIN("tb_category as c on a.cat_id = c.cat_id ");
            WHERE("c.cat_title = #{title}");
        }}.toString();
    }

    public String deleteById(Integer id){
        return new SQL(){{
            DELETE_FROM("tb_category");
            WHERE("cat_id = #{id}");
        }}.toString();
    }

    public String update(Category category){
        return new SQL(){{
            UPDATE("tb_category");
            SET("cat_title = " + "'" + category.getCat_title() + "'");
            WHERE("cat_id = " + "'" + category.getCat_id() + "'");
        }}.toString();
    }
}
