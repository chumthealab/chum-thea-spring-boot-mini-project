package com.hrd.miniproject.repositories;

import com.hrd.miniproject.models.Article;
import com.hrd.miniproject.models.Category;
import com.hrd.miniproject.repositories.providers.CategoryProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @SelectProvider(method = "findAll", type = CategoryProvider.class)
    public List<Category> findAll();

    @SelectProvider(method = "insert", type = CategoryProvider.class)
    public void insert(Category category);


    @SelectProvider(method = "findById", type = CategoryProvider.class)
    public Category findById(Integer id);


    @SelectProvider(method = "findArticleByCatTitle", type = CategoryProvider.class)
    @Results({
            @Result(property = "category.cat_id", column = "cat_id"),
            @Result(property = "category.cat_title", column = "cat_title")
    })
    public List<Article> findArticleByCatTitle(String title);

    @SelectProvider(method= "deleteById", type = CategoryProvider.class)
    void deleteById(Integer id);

    @SelectProvider(method = "update", type = CategoryProvider.class)
    void update(Category category);
}
