package com.hrd.miniproject.repositories;

import com.hrd.miniproject.models.Article;
import com.hrd.miniproject.repositories.providers.ArticleProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {

    @SelectProvider(method = "findAll", type = ArticleProvider.class)
    @Results({
            @Result(property = "category.cat_id", column = "cat_id"),
            @Result(property = "category.cat_title", column = "cat_title")
    })
    List<Article> findAll();


    @SelectProvider(method = "insert", type = ArticleProvider.class)
    void insert(Article article);

    @SelectProvider(method = "findById", type = ArticleProvider.class)
    @Results({
            @Result(property = "category.cat_id", column = "cat_id"),
            @Result(property = "category.cat_title", column = "cat_title")
    })
    Article findById(Integer id);

    @SelectProvider(method = "deleteById", type = ArticleProvider.class)
    void deleteById(Integer id);

    @SelectProvider(method = "update", type = ArticleProvider.class)
    void update(Article article);

    @SelectProvider(method = "findArticleByTitle", type = ArticleProvider.class)
    @Results({
            @Result(property = "category.cat_id", column = "cat_id"),
            @Result(property = "category.cat_title", column = "cat_title")
    })
    List<Article> findArticleByTitle(String title);

    @SelectProvider(method = "findArticleByTitleCat", type = ArticleProvider.class)
    @Results({
            @Result(property = "category.cat_id", column = "cat_id"),
            @Result(property = "category.cat_title", column = "cat_title")
    })
    List<Article> findArticleByTitleCat(@Param("title") String title, @Param("catId") Integer catId, @Param("currentPage") Integer currentPage, @Param("limitRow") Integer limitRow);


    @SelectProvider(method = "findArticleByTitleCatAll", type = ArticleProvider.class)
    @Results({
            @Result(property = "category.cat_id", column = "cat_id"),
            @Result(property = "category.cat_title", column = "cat_title")
    })
    List<Article> findArticleByTitleCatAll(@Param("title") String title, @Param("catId") Integer catId);


    ///////////////////////////////////////////////////////////////////////////////////////////////////


    @SelectProvider(method = "articlePage", type = ArticleProvider.class)
    @Results({
            @Result(property = "category.cat_id", column = "cat_id"),
            @Result(property = "category.cat_title", column = "cat_title")
    })
    List<Article> articlePage(@Param("currentPage") Integer currentPage, @Param("limitRow") Integer limitRow);
}
